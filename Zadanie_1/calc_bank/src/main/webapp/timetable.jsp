<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kalkulator bankowy</title>

<style>
	td {
	    border: 1px solid #000;
    	padding: 10px;
    }
</style>
</head>
<body>
<h1>Harmonogram spłat</h1>
<table>
	<thead>
		<tr>
			<td>Nr. raty</td>	
			<td>Kwota kapitału</td>	
			<td>Kwota odesetek</td>	
			<td>Opłaty stałe</td>	
			<td>Całkowita kwota raty</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach items = "${list}" var = "item">
			<tr>
				<td>${item.numer_raty}</td>
				<td>${item.kwota_kapitału}</td>
				<td>${item.kwota_odsetek}</td>
				<td>${item.oplaty_stale}</td>
				<td>${item.calwita_kwota}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
</body>
</html>