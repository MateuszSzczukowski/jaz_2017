<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kalkulator bankowy</title>
</head>
<body>
	<form action="timetable" method="POST">
		<label>
			<span>Kwota kredytu:</span>
			<input type="number" name="credit"/>
		</label>
		<br />
		<label>
			<span>Ilość rat</span>
			<input type="number" name="number_of_installment"/>
		</label>
		<br />
		<label>
			<span>Oprocentowanie</span>
			<input type="number" name="percent"/>
		</label>
		<br />
		<label>
			<span>Opłata stała</span>
			<input type="number" name="fixed_fee"/>
		</label>
		<br />
		<label>
			<span>Rodzaj raty</span>
			<select name="type_of_installment">
				<option value="malejaca">malejąca</option>
				<option value="stala">stała</option>
			</select>
		</label>
		<br />
		<input type="submit" value="Wyślij"/>
	</form>	
</body>
</html>