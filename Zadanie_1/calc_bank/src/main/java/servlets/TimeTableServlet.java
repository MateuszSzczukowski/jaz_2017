package servlets;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/timetable")
public class TimeTableServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String creditS = request.getParameter("credit");
		String number_of_installmentS = request.getParameter("number_of_installment");
		String percentS = request.getParameter("percent");
		String fixed_feeS = request.getParameter("fixed_fee");
		String type_of_installment = request.getParameter("type_of_installment");
		
		if(
				creditS == null || creditS.equals("") || 
				number_of_installmentS == null || number_of_installmentS.equals("") ||
				percentS == null || percentS.equals("") ||
				fixed_feeS == null || fixed_feeS.equals("") ||
				type_of_installment == null || type_of_installment.equals("")
		) {
			response.sendRedirect("/");
		} 
		int credit = Integer.parseInt(request.getParameter("credit"));
		int number_of_installment = Integer.parseInt(request.getParameter("number_of_installment"));
		int percent = Integer.parseInt(request.getParameter("percent"));
		int fixed_fee = Integer.parseInt(request.getParameter("fixed_fee"));

		if(
				credit <= 0 || 
				number_of_installment <= 0 ||
				percent <= 0 ||
				fixed_fee <= 0
		) {
			response.sendRedirect("/");
		} 
		
		
		int [] wyniki = calcInstallment(credit, number_of_installment, percent, fixed_fee);
		
		List <CreditItem> creditItem = new ArrayList<CreditItem>(number_of_installment); 
		
		for(int i=0; i<number_of_installment; i++) {
			creditItem.add( new CreditItem ((i+1),wyniki[0], wyniki[1], wyniki[2], wyniki[3]));
		}

		request.setAttribute("list", creditItem);
		RequestDispatcher rd = request.getRequestDispatcher("/timetable.jsp");
	    rd.forward(request,response);
	}
	
	public int [] calcInstallment(int credit, int number_of_installment, int percent, int fixed_fee) {
		int [] result = new int [4]; 
		
		int kwota_kapitalu = 0;
		int kwota_odsetek = 0;
		int oplaty_stale = 0;
		int calkowita_kwota = 0;
		
		kwota_kapitalu = credit/number_of_installment;
		kwota_odsetek = (kwota_kapitalu*percent)/100;
		oplaty_stale = fixed_fee;
		calkowita_kwota = kwota_kapitalu + kwota_odsetek + oplaty_stale;
		
		result[0] = kwota_kapitalu;
		result[1] = kwota_odsetek;
		result[2] = oplaty_stale;
		result[3] = calkowita_kwota;
		
		return result;
	}
}
