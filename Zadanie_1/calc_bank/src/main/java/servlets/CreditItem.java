package servlets;

public class CreditItem {

	public CreditItem(int numer_raty, int kwota_kapitału, int kwota_odsetek, int oplaty_stale, int calwita_kwota) {
		this.numer_raty = numer_raty;
		this.kwota_kapitału = kwota_kapitału;
		this.kwota_odsetek = kwota_odsetek;
		this.oplaty_stale = oplaty_stale;
		this.calwita_kwota = calwita_kwota;
	}
	
	private int numer_raty;
	private int kwota_kapitału;
	private int kwota_odsetek;
	private int oplaty_stale;
	private int calwita_kwota;
	
	public int getNumer_raty() {
		return numer_raty;
	}
	public void setNumer_raty(int numer_raty) {
		this.numer_raty = numer_raty;
	}
	public int getKwota_kapitału() {
		return kwota_kapitału;
	}
	public void setKwota_kapitału(int kwota_kapitału) {
		this.kwota_kapitału = kwota_kapitału;
	}
	public int getKwota_odsetek() {
		return kwota_odsetek;
	}
	public void setKwota_odsetek(int kwota_odsetek) {
		this.kwota_odsetek = kwota_odsetek;
	}
	public int getOplaty_stale() {
		return oplaty_stale;
	}
	public void setOplaty_stale(int oplaty_stale) {
		this.oplaty_stale = oplaty_stale;
	}
	public int getCalwita_kwota() {
		return calwita_kwota;
	}
	public void setCalwita_kwota(int calwita_kwota) {
		this.calwita_kwota = calwita_kwota;
	}
	@Override
	public String toString() {
		return "CreditItem [numer_raty=" + numer_raty + ", kwota_kapitału=" + kwota_kapitału + ", kwota_odsetek="
				+ kwota_odsetek + ", oplaty_stale=" + oplaty_stale + ", calwita_kwota=" + calwita_kwota + "]";
	}
}
